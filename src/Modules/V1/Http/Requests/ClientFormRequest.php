<?php
namespace Modules\V1\Http\Requests;

use SoliDry\Extension\BaseFormRequest;

class ClientFormRequest extends BaseFormRequest 
{
    // >>>props>>>
    public $id;
    // Attributes
    public $KodClient;
    public $SetByClient;
    public $Name;
    public $City;
    public $FullTel;
    public $KodPriority;
    public $Loyalty;
    public $Discount;
    public $OrdMinText;
    public $DatePrice;
    public $PriceActual;
    public $PricePublish_ID;
    // <<<props<<<

    // >>>methods>>>
    public function authorize(): bool 
    {
        return true;
    }

    public function rules(): array 
    {
        return [
            'KodClient' => '',
            'SetByClient' => '',
            'Name' => 'string',
            'City' => 'string',
            'FullTel' => 'string',
            'KodPriority' => '',
            'Loyalty' => '',
            'Discount' => '',
            'OrdMinText' => 'string',
            'DatePrice' => 'string',
            'PriceActual' => 'string',
            'PricePublish_ID' => '',
        ];
    }

    public function relations(): array 
    {
        return [

        ];
    }
    // <<<methods<<<
}
