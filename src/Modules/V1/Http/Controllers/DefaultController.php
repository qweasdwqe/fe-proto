<?php
namespace Modules\V1\Http\Controllers;

use SoliDry\Extension\BaseController;

class DefaultController extends BaseController 
{
    // >>>props>>>
    // <<<props<<<
    // >>>methods>>>
    /**
    * @OA\Info(
    *     title="FlowExpert API",
    *     version="1",
    *     @OA\Contact(
    *     ),
    *     @OA\License(
    *     ),
    * )
    */
    // <<<methods<<<
}
