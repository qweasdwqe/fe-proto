<?php
namespace Modules\V1\Http\Controllers;

class ClientController extends DefaultController 
{
    // >>>props>>>
    // <<<props<<<
    // >>>methods>>>
    /**
    * @OA\Get(
    *     path="/v1/client",
    *     summary="Get Clients ",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="query",
    *         name="include",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="page",
    *         required=false,
    *         @OA\Schema(
    *             type="integer",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="limit",
    *         required=false,
    *         @OA\Schema(
    *             type="integer",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="sort",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="data",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="filter",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="order_by",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Get(
    *     path="/v1/client/{id}",
    *     summary="Get Client",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="query",
    *         name="include",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="query",
    *         name="data",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Post(
    *     path="/v1/client",
    *     summary="Create Client",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="201",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Patch(
    *     path="/v1/client/{id}",
    *     summary="Update Client",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Delete(
    *     path="/v1/client/{id}",
    *     summary="Delete Client",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="204",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Get(
    *     path="/v1/client/{id}/{related}",
    *     summary="Get Client related objects",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="query",
    *         name="data",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="id",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="related",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Get(
    *     path="/v1/client/{id}/relationships/{relations}",
    *     summary="Get Client relations objects",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="query",
    *         name="data",
    *         required=false,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="id",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="relations",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Post(
    *     path="/v1/client/{id}/relationships/{relations}",
    *     summary="Create Client relation object",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="path",
    *         name="id",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="relations",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="201",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Patch(
    *     path="/v1/client/{id}/relationships/{relations}",
    *     summary="Update Client relation object",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="path",
    *         name="id",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="relations",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Delete(
    *     path="/v1/client/{id}/relationships/{relations}",
    *     summary="Delete Client relation object",
    *     tags={"ClientController"},
    *     @OA\Parameter(
    *         in="path",
    *         name="id",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Parameter(
    *         in="path",
    *         name="relations",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *         ),
    *     ),
    *     @OA\Response(
    *         response="204",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Post(
    *     path="/v1/client/bulk",
    *     summary="Create Client bulk",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="201",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Patch(
    *     path="/v1/client/bulk",
    *     summary="Update Client bulk",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="200",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    /**
    * @OA\Delete(
    *     path="/v1/client/bulk",
    *     summary="Delete Client bulk",
    *     tags={"ClientController"},
    *     @OA\Response(
    *         response="204",
    *         description="successful operation",
    *         @OA\MediaType(
    *             mediaType="application/vnd.api+json"
    *         ),
    *     ),
    * )
    */

    // <<<methods<<<
}
