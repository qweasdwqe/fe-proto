<?php
// >>>routes>>>
// Client routes
Route::group(['prefix' => 'v1', 'namespace' => '\\Modules\\V1\\Http\\Controllers'], function()
{
    // bulk routes
    Route::post('/client/bulk', 'ClientController@createBulk');
    Route::patch('/client/bulk', 'ClientController@updateBulk');
    Route::delete('/client/bulk', 'ClientController@deleteBulk');
    // basic routes
    Route::options('/client', 'ClientController@options');
    Route::get('/client', 'ClientController@index');
    Route::get('/client/{id}', 'ClientController@view');
    Route::post('/client', 'ClientController@create');
    Route::patch('/client/{id}', 'ClientController@update');
    Route::delete('/client/{id}', 'ClientController@delete');
    // relation routes
    Route::get('/client/{id}/{related}', 'ClientController@related');
    Route::get('/client/{id}/relationships/{relations}', 'ClientController@relations');
    Route::post('/client/{id}/relationships/{relations}', 'ClientController@createRelations');
    Route::patch('/client/{id}/relationships/{relations}', 'ClientController@updateRelations');
    Route::delete('/client/{id}/relationships/{relations}', 'ClientController@deleteRelations');
});
// <<<routes<<<
