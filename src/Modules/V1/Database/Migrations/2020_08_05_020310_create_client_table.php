<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration 
{
    public function up() 
    {
        Schema::create('client', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Name');
            $table->string('City');
            $table->string('FullTel');
            $table->string('OrdMinText');
            $table->string('DatePrice');
            $table->string('PriceActual');
            $table->timestamps();
        });
    }

    public function down() 
    {
        Schema::dropIfExists('client');
    }

}
