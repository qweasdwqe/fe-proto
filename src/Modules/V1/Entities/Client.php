<?php
namespace Modules\V1\Entities;

use SoliDry\Extension\BaseModel;

class Client extends BaseModel 
{
    // >>>props>>>
    protected $primaryKey = 'id';
    protected $table = 'client';
    public $timestamps = false;
    // <<<props<<<
    // >>>methods>>>

    // <<<methods<<<
}
